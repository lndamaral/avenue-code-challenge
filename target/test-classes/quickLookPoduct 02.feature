Feature: 
As a customer
I want to search for a product and open it's quick look overlay
I should be able to see the exact same clicked product on the quick look overlay

Scenario: See a Product on Quick Look Overlay
Given I search "knife" in home page 
When I click in Quick Look for "Shun Kaji Western Chef's Knife"
Then this product, "Shun Kaji Western Chef's Knife", will be shown in quick look overlay with the same name and price, "Our Price: $129.95 – $259.95", than previous page