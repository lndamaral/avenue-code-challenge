Feature: 
As a customer
I want to search for a product and open it's quick look overlay
I should be able to see the exact same clicked product on the quick look overlay

Scenario: See a Product on Quick Look Overlay
Given I search "fry pan" in home page 
When I click in Quick Look for "Le Creuset Signature Cast-Iron Fry Pan"
Then this product, "Le Creuset Signature Cast-Iron Fry Pan", will be shown in quick look overlay with the same name and price, "Our Price: $160 – $190", than previous page