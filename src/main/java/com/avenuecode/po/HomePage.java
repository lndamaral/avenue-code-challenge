/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.po;

import com.avenuecode.miscellaneous.AvenueCodeUtils;
import cucumber.annotation.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    WebDriver driver = null;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void menu(String menu) {
        AvenueCodeUtils.dragToElement(driver, driver.findElement(By.linkText(menu.split(",")[0])));
        driver.findElement(By.linkText(menu.split(", ")[1])).click();
    }

    public void search(String content) {
        driver.findElement(By.id("search-field")).sendKeys(content);
        driver.findElement(By.id("btnSearch")).click();
    }
}
