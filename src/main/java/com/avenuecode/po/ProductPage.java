/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.po;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author leonardo
 */
public class ProductPage {

    WebDriver driver = null;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectSize(String size) {

        List<WebElement> elements = driver.findElements(By.cssSelector("li[class='attributeValue'] > a"));

        for (WebElement e : elements) {
            if (e.getText().contains(size)) {
                e.click();
                break;
            }
        }

    }

    public void color(String color){
        driver.findElement(By.cssSelector("a[title=' "+color+" ']")).click();
    }
    
    public void quantity(String quantity) {
        driver.findElement(By.cssSelector("input[class='qty']")).clear();
        driver.findElement(By.cssSelector("input[class='qty']")).sendKeys(quantity);
    }

    public void addToCart() {
        driver.findElement(By.cssSelector("button[class='btn btn_addtobasket btn_addtobasket_add']")).click();
    }

    public void checkout() {
        driver.findElement(By.cssSelector("div[id='btn-checkout'] > a")).click();
    }
}
