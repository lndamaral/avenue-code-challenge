Feature: 
As a customer
I want to go to a product page and add the product to the cart
I should be able to see this product added to cart on shopping cart page

Scenario: Do Addind a product to car
Given I access the menu "Bakeware, Le Creuset"
When I select the product "Le Creuset Café Stoneware French Press" and 
When I select the color "Red" and 
When I select "2" of quantity and
When I confirm this operation
Then this product, "Bakeware, Le Creuset", will be added in the shopping cart page