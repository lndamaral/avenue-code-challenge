/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.po;

import com.avenuecode.miscellaneous.AvenueCodeUtils;
import cucumber.annotation.en.When;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author leonardo
 */
public class GroupProductPage {

    WebDriver driver = null;
    String price = null;

    public GroupProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectProduct(String productName) {
        driver.findElement(By.partialLinkText(productName)).click();
    }

    public void quickLook(String productName) {

        List<WebElement> element = driver.findElements(By.cssSelector("li[class='product-cell ']"));

        for (WebElement e : element) {
            if (e.getText().contains(productName)) {
                price = e.findElement(By.cssSelector("span[class='price-state price-special']")).getText();
                AvenueCodeUtils.dragToElement(driver, e.findElement(By.cssSelector("div[class='product-thumb']")));
                e.findElement(By.cssSelector("a > span[class='quicklook-link']")).click();
            }
        }

    }

    public String getPrice() {
        return price;
    }

}
