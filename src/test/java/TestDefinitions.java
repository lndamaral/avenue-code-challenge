/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.avenuecode.po.HomePage;
import com.avenuecode.po.GroupProductPage;
import com.avenuecode.po.ProductPage;
import com.avenuecode.po.QuickLookPage;
import com.avenuecode.po.ShoppingCart;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestDefinitions {

    protected WebDriver driver;

    public TestDefinitions() {
        driver = new FirefoxDriver();
        driver.get("http://www.williams-sonoma.com");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Given("^I access the menu \"([^\"]*)\"$")
    public void menu(String menu) {
        new HomePage(driver).menu(menu);
    }

    @When("^I select the product \"([^\"]*)\" and$")
    public void I_select_the_product(String product) {
        new GroupProductPage(driver).selectProduct(product);
    }

    @When("^I select the size \"([^\"]*)\" and$")
    public void I_select_the_size_and(String size) {
        new ProductPage(driver).selectSize(size);
    }

    @When("^I select \"([^\"]*)\" of quantity and$")
    public void I_select_of_quantity_and(String qtn) {
        new ProductPage(driver).quantity(qtn);
    }

    @When("^I confirm this operation$")
    public void I_confirm_this_operation() {
        new ProductPage(driver).addToCart();
        new ProductPage(driver).checkout();
    }

    @When("^I select the color \"([^\"]*)\" and$")
    public void I_select_color(String color) {
        new ProductPage(driver).color(color);
    }

    @Then("^this product, \"([^\"]*)\", will be added in the shopping cart page$")
    public void this_product_will_be_added_in_the_shopping_cart_page(String product) {
        assertEquals(product, new ShoppingCart(driver).getProduct());
    }

    @Given("^I search \"([^\"]*)\" in home page$")
    public void I_search_in_home_page(String content) {
        new HomePage(driver).search(content);
    }

    @When("^I click in Quick Look for \"([^\"]*)\"$")
    public void I_click_in_Quick_Look_for(String product) {
        new GroupProductPage(driver).quickLook(product);
    }

    @Then("^this product, \"([^\"]*)\", will be shown in quick look overlay with the same name and price, \"([^\"]*)\", than previous page$")
    public void this_product_will_be_shown_in_quick_look_overlay_with_the_same_name_and_price_than_previous_page(String product, String price) {
        assertEquals(product, new QuickLookPage(driver).getName());
        assertEquals(price, new QuickLookPage(driver).getPrice());
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
//    //@Test
//    public void secondOne() {
//        WebDriver driver = new FirefoxDriver();
//        driver.get("http://www.williams-sonoma.com");
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//
//        HomePage home = new HomePage(driver);
//        home.search("fry pan");
//
//        GroupProductPage gp = new GroupProductPage(driver);
//        gp.quickLook("Le Creuset Signature Cast-Iron Fry Pan");
//
//        QuickLookPage ql = new QuickLookPage(driver);
//
//        assertEquals("Le Creuset Signature Cast-Iron Fry Pan", ql.getName());
//        assertEquals(gp.getPrice(), ql.getPrice());
//
//    }
//
//    //@Test
//    public void firstOne() {
//
//        WebDriver driver = new FirefoxDriver();
//
//        driver.get("http://www.williams-sonoma.com");
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//
//        HomePage hp = new HomePage(driver);
//        GroupProductPage groupProduct = new GroupProductPage(driver);
//        ProductPage product = new ProductPage(driver);
//        ShoppingCart cart = new ShoppingCart(driver);
//
////        hp.menu("Cutlery", "Consigli");
////        groupProduct.selectProduct("Consigli Olivewood Chef's Knife");
//        product.selectSize("9\"");
//        product.quantity("2");
//        product.addToCart();
//        product.checkout();
//
//        assertEquals("Consigli Olivewood Chef's Knife", cart.getProduct());
//
//    }
}
