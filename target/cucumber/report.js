$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('quickLookPoduct.feature');
formatter.feature({
  "id": "",
  "description": "As a customer\nI want to search for a product and open it\u0027s quick look overlay\nI should be able to see the exact same clicked product on the quick look overlay",
  "name": "",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": ";see-a-product-on-quick-look-overlay",
  "description": "",
  "name": "See a Product on Quick Look Overlay",
  "keyword": "Scenario",
  "line": 6,
  "type": "scenario"
});
formatter.step({
  "name": "I search \"fry pan\" in home page",
  "keyword": "Given ",
  "line": 7
});
formatter.step({
  "name": "I click in Quick Look for \"Le Creuset Signature Cast-Iron Fry Pan\"",
  "keyword": "When ",
  "line": 8
});
formatter.step({
  "name": "this product, \"Le Creuset Signature Cast-Iron Fry Pan\", will be shown in quick look overlay with the same name and price, \"Our Price: $160 – $190\", than previous page",
  "keyword": "Then ",
  "line": 9
});
formatter.match({
  "arguments": [
    {
      "val": "fry pan",
      "offset": 10
    }
  ],
  "location": "TestDefinitions.I_search_in_home_page(String)"
});
formatter.result({
  "duration": 3364387058,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Le Creuset Signature Cast-Iron Fry Pan",
      "offset": 27
    }
  ],
  "location": "TestDefinitions.I_click_in_Quick_Look_for(String)"
});
formatter.result({
  "duration": 95516838478,
  "status": "failed",
  "error_message": "org.openqa.selenium.StaleElementReferenceException: Element not found in the cache - perhaps the page has changed since it was looked up\nCommand duration or timeout: 10.03 seconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/stale_element_reference.html\nBuild info: version: \u00272.44.0\u0027, revision: \u002776d78cf323ce037c5f92db6c1bba601c2ac43ad8\u0027, time: \u00272014-10-23 13:11:40\u0027\nSystem info: host: \u0027leonardo-amaral\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00273.13.0-43-generic\u0027, java.version: \u00271.7.0_72\u0027\nSession ID: 77651d8e-24f6-41ed-8c95-fd61e8ceaf49\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities [{platform\u003dLINUX, acceptSslCerts\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, databaseEnabled\u003dtrue, browserName\u003dfirefox, handlesAlerts\u003dtrue, nativeEvents\u003dfalse, webStorageEnabled\u003dtrue, rotatable\u003dfalse, locationContextEnabled\u003dtrue, applicationCacheEnabled\u003dtrue, takesScreenshot\u003dtrue, version\u003d34.0}]\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:57)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:526)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:268)\n\tat org.openqa.selenium.remote.RemoteWebElement.getText(RemoteWebElement.java:152)\n\tat com.avenuecode.po.GroupProductPage.quickLook(GroupProductPage.java:37)\n\tat TestDefinitions.I_click_in_Quick_Look_for(TestDefinitions.java:94)\n\tat ✽.When I click in Quick Look for \"Le Creuset Signature Cast-Iron Fry Pan\"(quickLookPoduct.feature:8)\nCaused by: org.openqa.selenium.StaleElementReferenceException: Element not found in the cache - perhaps the page has changed since it was looked up\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/stale_element_reference.html\nBuild info: version: \u00272.44.0\u0027, revision: \u002776d78cf323ce037c5f92db6c1bba601c2ac43ad8\u0027, time: \u00272014-10-23 13:11:40\u0027\nSystem info: host: \u0027leonardo-amaral\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00273.13.0-43-generic\u0027, java.version: \u00271.7.0_72\u0027\nDriver info: driver.version: unknown\n\tat \u003canonymous class\u003e.fxdriver.cache.getElementAt(resource://fxdriver/modules/web-element-cache.js:8329:1)\n\tat \u003canonymous class\u003e.Utils.getElementAt(file:///tmp/anonymous1546048234517995033webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:7922:10)\n\tat \u003canonymous class\u003e.WebElement.getElementText(file:///tmp/anonymous1546048234517995033webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:11065:11)\n\tat \u003canonymous class\u003e.DelayedCommand.prototype.executeInternal_/h(file:///tmp/anonymous1546048234517995033webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:11635:16)\n\tat \u003canonymous class\u003e.fxdriver.Timer.prototype.setTimeout/\u003c.notify(file:///tmp/anonymous1546048234517995033webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:548:5)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "Le Creuset Signature Cast-Iron Fry Pan",
      "offset": 15
    },
    {
      "val": "Our Price: $160 – $190",
      "offset": 123
    }
  ],
  "location": "TestDefinitions.this_product_will_be_shown_in_quick_look_overlay_with_the_same_name_and_price_than_previous_page(String,String)"
});
formatter.result({
  "status": "skipped"
});
});