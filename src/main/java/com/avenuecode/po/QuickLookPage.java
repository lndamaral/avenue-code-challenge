/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author leonardo
 */
public class QuickLookPage {

    WebDriver driver = null;

    public QuickLookPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPrice() {
        return "Our Price: $" + driver.findElement(By.cssSelector("span[itemprop='lowPrice']")).getText()
                + " – $"
                + driver.findElement(By.cssSelector("span[itemprop='highPrice']")).getText();
    }

    public String getName() {
        return driver.findElement(By.cssSelector("h1[itemprop='name']")).getText();
    }
}
