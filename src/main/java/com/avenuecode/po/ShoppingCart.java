/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author leonardo
 */
public class ShoppingCart {

    WebDriver driver = null;

    public ShoppingCart(WebDriver driver) {
        this.driver = driver;
    }

    public String getProduct() {
        return driver.findElement(By.cssSelector("li[class='title'] > a")).getText().split(",")[0];
    }
}
